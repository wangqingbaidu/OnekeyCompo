This research explores the contrasts between traditional omics techniques and the evaluation of delta Radiomics, highlighting the unique impacts of each method on the efficacy of the overall model. The model adeptly integrates post-treatment (post) and pre-treatment (pre) imaging features with molecular data, providing a robust tool for enhanced disease prognosis and management. Figure 1 illustrates the workflow of this comprehensive methodology, showcasing the integration of post-pre characteristics to harness temporal changes for predictive accuracy.

Fig1. Workflow of this study.

## Methods

### Patients

> 这里填一下你的入组出组标准

This study divided the samples into two cohorts: a training cohort, comprising 70% of the data, and an internal validation cohort, consisting of the remaining 30%. 

### Image Acquisition

> 数据获取对应的参数，参考一下下面这个描述


**Image Segmentation**: Image segmentation was conducted by a board-certified radiologist (S.K.) with five years of experience in rectal MRI, using 3D Slicer version 4.10 (www.slicer.org), a free and open-source software. The radiologist semi-automatically segmented the entire area within the rectal wall post-treatment, excluding ambiguous normal rectal wall and mucosal edema, on high-spatial-resolution axial T2-weighted images. Both segmentation and subsequent feature extraction utilized post-nCRT MRI, while pre-nCRT MRI served to identify the baseline tumor extent. All segmentation masks were validated by a senior radiologist (J.S.L.), with any disagreements resolved through consensus-based discussions. The radiologists were blinded to clinical and histopathologic data, except for the diagnosis of rectal cancer. Segmentation masks from T2-weighted images were then registered to the apparent diffusion coefficient (ADC) maps.

### Radiomics Procedure

#### Feature Extraction


Radiomic feature extraction was conducted using the PyRadiomics open-source Python package (version 3.7.12; https://pyradiomics.readthedocs.io) on the OnekeyAI platform. This process involved extracting features from both T2-weighted images and ADC maps obtained from the MRI scans. All available features implemented in PyRadiomics were extracted from the original images and filtered images, including wavelet and Laplacian of Gaussian transformations. Prior to feature extraction, z score normalization was applied only to the MRI signal intensities of the T2-weighted images. Gray-level discretization with fixed bin width values of 3 for T2-weighted images and 20 for ADC maps, as well as voxel size resampling by $1 * 1 * 1$ mm, were also performed using PyRadiomics. As a result, a total of 1834 features were obtained for each T2-weighted image and ADC map.


**Feature Early Fusion**: We integrated $features_{pre}$, $features_{post}$ using a feature concatenation method ($\ominus$, $\otimes$), combining these into a single comprehensive feature vector:
$$
feature_{delta} = features_{post} \ominus features_{pre}
$$

$$
feature_{fusion} = features_{post} \otimes features_{pre}
$$

where $\ominus$ represents element-wise subtraction, and $\otimes$ denotes feature concatenation.

#### Feature Selection


To ensure feature reproducibility, we first excluded features with intraclass correlation coefficients (ICCs) below 0.7. We then conducted a U test (p < 0.01) to select features with significant differences between osteoporosis and non-osteoporosis groups. Multi-collinearity was assessed using Spearman correlation analysis, with pairs of features exhibiting a correlation coefficient ≥0.9 or ≤-0.9 reduced to retain only the feature with better diagnostic performance. Hierarchical clustering was applied to further reduce redundancy, consolidating clusters with a correlation coefficient greater than 0.95 into a single representative feature based on the largest dynamic range.  Subsequently, the least absolute shrinkage and selection operator (LASSO) logistic regression was employed to refine the feature set. We used 10-fold cross-validation to determine the optimal regularization parameter λ, based on minimizing binomial deviance. Utilizing these selected features, we built machine learning models and computed a radiomics score for each patient as a weighted linear combination of the chosen features. Classification performance was evaluated on models derived from each modal and combination of all modals.

#### Signature Building


**Delta Radiomics Signature**: Utilizing LASSO for rigorous feature selection, we developed a radiomics risk model through machine learning algorithms, including Logistic Regression, Support Vector Machine, and Random Forest. Comparative analysis was conducted to assess the performance of each model. Additionally, we explored the benefits of feature fusion by integrating multimodal features, demonstrating the advantages of combining various imaging modalities to enhance predictive accuracy.

**Pre and Post Radiomics Signature**: Similar to delta radiomics, we extracted features from imaging data before and after treatment. These features were then subjected to feature selection and dimensionality reduction to derive the Pre and Post Radiomics Signatures. By comparing these with the Delta model, we validated the effectiveness of the Delta approach in enhancing model robustness and predictive power.

### Metrics


The diagnostic efficacy of our deep learning model was rigorously evaluated in the test cohort through the construction of Receiver Operating Characteristic (ROC) curves to assess its discriminative ability. Calibration of the model was analyzed using calibration curves, complemented by the Hosmer-Lemeshow goodness-of-fit test to validate its reliability. Additionally, Decision Curve Analysis (DCA) was employed to appraise the clinical utility of our predictive models, facilitating an understanding of the potential benefits in a clinical context.

### Statistical Analysis


The normality of clinical features was verified using the Shapiro-Wilk test. Continuous variables were subjected to the t-test or the Mann-Whitney U test based on their distribution characteristics. Categorical variables were examined using Chi-square (χ²) tests. The baseline characteristics across all cohorts are detailed in Table 1. Notably, p-values exceeding 0.05 between cohorts indicated no significant differences, thus confirming an unbiased division between groups.

All data analyses were executed on the OnekeyAI platform version 4.9.1 utilizing Python 3.7.12. Statistical assessments were conducted with Statsmodels version 0.13.2. Radiomics feature extraction was performed via PyRadiomics version 3.0.1. Machine learning implementations, including the Support Vector Machine (SVM), were facilitated using Scikit-learn version 1.0.2. Our deep learning frameworks were developed on PyTorch version 1.11.0, optimized for enhanced performance through CUDA version 11.3.1 and cuDNN version 8.2.1.


| feature_name   | -label=ALL   | -label=test   | -label=train   | pvalue   |
|:---------------|:-------------|:--------------|:---------------|:---------|
| age            | 44.88±17.03  | 45.69±17.69   | 44.52±16.78    | 0.643    |
| BMI            | 23.10±2.50   | 22.96±2.89    | 23.16±2.31     | 0.281    |
| chemotherapy   |              |               |                | 0.337    |
| 0              | 96(40.51)    | 33(45.83)     | 63(38.18)      |          |
| 1              | 141(59.49)   | 39(54.17)     | 102(61.82)     |          |
| gender         |              |               |                | 0.082    |
| 0              | 20(8.44)     | 10(13.89)     | 10(6.06)       |          |
| 1              | 217(91.56)   | 62(86.11)     | 155(93.94)     |          |
| degree         |              |               |                | 0.483    |
| 0              | 53(22.36)    | 14(19.44)     | 39(23.64)      |          |
| 1              | 182(76.79)   | 58(80.56)     | 124(75.15)     |          |
| 2              | 2(0.84)      |               | 2(1.21)        |          |
| Tstage         |              |               |                | 0.127    |
| 0              | 52(21.94)    | 12(16.67)     | 40(24.24)      |          |
| 1              | 59(24.89)    | 17(23.61)     | 42(25.45)      |          |
| 2              | 65(27.43)    | 27(37.50)     | 38(23.03)      |          |
| 3              | 61(25.74)    | 16(22.22)     | 45(27.27)      |          |
| smoke          |              |               |                | 0.607    |
| 0              | 59(24.89)    | 20(27.78)     | 39(23.64)      |          |
| 1              | 178(75.11)   | 52(72.22)     | 126(76.36)     |          |
| drink          |              |               |                | 0.588    |
| 0              | 78(32.91)    | 26(36.11)     | 52(31.52)      |          |
| 1              | 159(67.09)   | 46(63.89)     | 113(68.48)     |          |

Table1. baseline characters of our cohorts.

## Results

### Clinical Features

**Univariable Analysis**: In our study, we performed a detailed univariate analysis of all clinical features, emphasizing the calculation of Odds Ratios (OR) and associated p-values for each variable. This ratio played a critical role in the development of our final fusion model. In our experiment, significant features identified through univariable screening were employed in constructing the Clinical Signature.

| feature_name   |   OR_UNI |   OR lower 95%CI_UNI |   OR upper 95%CI_UNI |   p_value_UNI |   OR_MULTI |   OR lower 95%CI_MULTI |   OR upper 95%CI_MULTI |   p_value_MULTI |
|:---------------|---------:|---------------------:|---------------------:|--------------:|-----------:|-----------------------:|-----------------------:|----------------:|
| drink          |    0.378 |                0.267 |                0.535 |         0     |      0.352 |                  0.165 |                  0.754 |           0.024 |
| chemotherapy   |    0.437 |                0.306 |                0.622 |         0     |      0.649 |                  0.35  |                  1.203 |           0.249 |
| degree         |    0.447 |                0.327 |                0.612 |         0     |      0.959 |                  0.492 |                  1.868 |           0.918 |
| gender         |    0.462 |                0.348 |                0.614 |         0     |      1.378 |                  0.408 |                  4.655 |           0.665 |
| smoke          |    0.482 |                0.353 |                0.66  |         0     |      2.139 |                  0.915 |                  5.003 |           0.141 |
| Tstage         |    0.794 |                0.689 |                0.914 |         0.007 |      1.329 |                  1.015 |                  1.74  |           0.082 |
| BMI            |    0.967 |                0.955 |                0.978 |         0     |      0.937 |                  0.876 |                  1.003 |           0.116 |
| age            |    0.986 |                0.98  |                0.992 |         0     |      1.008 |                  0.991 |                  1.026 |           0.436 |

Table 2. Univariable Analysis of clinical features.

### Signature Comparison

这里可以添加指标解读

| Signature   |   Accuracy |   AUC | 95% CI          |   Sensitivity |   Specificity |   PPV |   NPV | Cohort   |
|:------------|-----------:|------:|:----------------|--------------:|--------------:|------:|------:|:---------|
| Clinic      |      0.685 | 0.565 | 0.4858 - 0.6438 |         0     |         1     | 0     | 0.685 | train    |
| PreRad      |      0.758 | 0.744 | 0.6601 - 0.8269 |         0.481 |         0.885 | 0.658 | 0.787 | train    |
| PostRad     |      0.758 | 0.744 | 0.6601 - 0.8269 |         0.481 |         0.885 | 0.658 | 0.787 | train    |
| DeltaRad    |      0.758 | 0.744 | 0.6601 - 0.8269 |         0.481 |         0.885 | 0.658 | 0.787 | train    |
| Combined    |      0.727 | 0.747 | 0.6618 - 0.8327 |         0.615 |         0.779 | 0.561 | 0.815 | train    |
| Clinic      |      0.681 | 0.522 | 0.4000 - 0.6444 |         0     |         1     | 0     | 0.681 | test     |
| PreRad      |      0.778 | 0.824 | 0.7085 - 0.9401 |         0.739 |         0.796 | 0.63  | 0.867 | test     |
| PostRad     |      0.778 | 0.824 | 0.7085 - 0.9401 |         0.739 |         0.796 | 0.63  | 0.867 | test     |
| DeltaRad    |      0.778 | 0.824 | 0.7085 - 0.9401 |         0.739 |         0.796 | 0.63  | 0.867 | test     |
| Combined    |      0.833 | 0.808 | 0.6843 - 0.9324 |         0.652 |         0.918 | 0.789 | 0.849 | test     |

Table3. Metrics on different signature.

<img src="img/train_auc.svg" style = "zoom: 50%" /><img src="img/test_auc.svg" style = "zoom: 50%" />

Fig 3. Different signatures‘ ROC on different cohort.

**Calibration Curve**: The Hosmer-Lemeshow (HL) test quantifies the discrepancy between predicted probabilities and observed outcomes; a lower HL statistic indicates better calibration.

<img src="img/train_cali.svg" style = "zoom: 50%" /><img src="img/test_cali.svg" style = "zoom: 50%" />

Fig 4. Calibration curves of different signatures for the test cohort.

**DeLong Test**: The DeLong test was applied to both training and testing sets to evaluate the statistical significance of differences between models. Our Combined model demonstrated a significant improvement over traditional omics and peritumoral area models. However, the improvement over the MIL model was not as pronounced. This could be attributed to the limited incremental information gain provided by the fusion of omics data with the MIL model.

<img src = "img/train_delong.svg" style = "zoom: 50%" /><img src = "img/test_delong.svg" style = "zoom: 50%" />

Figure 5. DeLong test results for different signatures.

### Clinical Use

**Decision Curve Analysis (DCA)**: Figure 6 presents the decision curve analysis (DCA) for both training and testing sets. The results indicate that our fusion model provides considerable advantages in terms of predicted probabilities. Furthermore, it consistently offers a greater potential for net benefit compared to other signatures, underscoring its effectiveness.

<img src = "img/train_dca.svg" style = "zoom: 50%" /><img src = "img/test_dca.svg" style = "zoom: 50%" />

Fig 6. Different signatures' decision curve on test cohort.

<img src = "img/nomogram.png" />

